-- Use most basic types so will be compatible with SQLite
-- All dates stored as timestamp in seconds, with INT

CREATE TABLE forum_targets (
    forum_id INT NOT NULL,
    forum_name TEXT NOT NULL DEFAULT '',
    last_success_crawl INT NOT NULL DEFAULT 0,
    most_recent_update INT NOT NULL DEFAULT 0,
    dirty_until_page INT NOT NULL DEFAULT 0,
    dirty_most_recent_update INT NOT NULL DEFAULT 0,
    successive_fail INT NOT NULL DEFAULT 0,
    parent_forum_id INT NOT NULL DEFAULT 0,
    display_order INT NOT NULL DEFAULT 0,
    refresh_interval INT NOT NULL DEFAULT 600,
    most_recent_update_include_children INT NOT NULL DEFAULT 0,
    PRIMARY KEY(forum_id)
);

CREATE TABLE thread_targets (
    thread_id INT NOT NULL,
    forum_id INT NOT NULL DEFAULT 0,
    thread_title TEXT NOT NULL DEFAULT '',
    last_success_crawl INT NOT NULL DEFAULT 0,
    last_page_number INT NOT NULL DEFAULT 0,
    most_recent_update INT NOT NULL DEFAULT 0,
    dirty_until_page INT NOT NULL DEFAULT 0,
    dirty_most_recent_update INT NOT NULL DEFAULT 0,
    successive_fail INT NOT NULL DEFAULT 0,
    poll_title TEXT NOT NULL DEFAULT '',
    thread_title_sc TEXT NOT NULL DEFAULT '',
    PRIMARY KEY(thread_id)
);

CREATE INDEX thread_targets_forum_id ON thread_targets USING btree (forum_id);

CREATE TABLE posts (
    post_id INT NOT NULL,
    thread_id INT NOT NULL DEFAULT 0,
    title TEXT NOT NULL DEFAULT '',
    user_name TEXT NOT NULL DEFAULT '',
    content TEXT NOT NULL DEFAULT '',
    posted_date INT NOT NULL DEFAULT 0,
    most_recent_update INT NOT NULL DEFAULT 0,
    content_sc TEXT NOT NULL DEFAULT '',
    PRIMARY KEY(post_id)
);

CREATE INDEX posts_thread_id ON posts USING btree (thread_id);
CREATE INDEX posts_user_name ON posts USING hash (user_name);

CREATE TABLE users (
    user_id INT NOT NULL,
    user_name TEXT NOT NULL DEFAULT '',
    registered_at INT NOT NULL DEFAULT 0,
    PRIMARY KEY(user_id)
);

CREATE INDEX user_name_exact ON users USING HASH (user_name);